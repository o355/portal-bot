# Portal Bot
Portal Bot is an automatic bot that checks portals based on Slate Technolutions & Salesforce, before uploading those images to Slack.

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

Portal Bot Spaghet-o-meter: **11/10**

Portal Bot is spaghetti code. Copypasta code litters the entire script. There's no modularity - hard coded values are used at all times. Salesforce portals & Slate Technolutions portals could have been done using object oriented programming, but instead it's all just spaghetti code. The configuration for which schools to visit should have also been achieved with something akin to a JSON or YAML file that the script then reads. Slack API uploads could have also been wrapped into a function for better readability & reusability.

Portal Bot is just bad. Really bad. It's awful.

While the main.py file is 150 lines long, the total lack of any modularity, and this literally being the easiest candidate to do object-oriented & reusable programing with bring the score to an 11/10.

# Identifying Slate Technoloutions and Salesforce portals
Slate Technoloution portals are usually a single long page with status updates, if forms were submitted, etc. They look like the demos on this page: https://technolutions.com/

Salesforce portals usually have the .force.com domain attached to them. They're usually pretty slow to load, too.

Some schools, like UC schools have custom portals. Portal Bot has the implementation for Northeastern's portal, but not others. Additionally, the implementation for CMU's portal is possible, so long as you don't use Google for authentication. I tried to implement portal bot with Google for CMU, and it didn't work at all.

# Requirements
This script runs on Python 3 and is heavily based on Selenium and Requests. This version of Portal Bot runs on Chrome driver, but can be adapted to Firefox driver very easily. Make sure you know your path to the chromedriver (or geckodriver).

You'll need a Slack channel for the bot to post in (#portals in the code), along with API keys for the bot. More about this momentarily.

You'll probably want some sort of timer to execute Portal Bot on a daily schedule. You can do this via cron. I personally chose 4:30 PM ET as a good time for portal checks, as this is when the business day ends for most colleges in the US.

# Note
This is the exact opposite of an easily extendable script. It's a quick thing I wrote in about 4-6 hours, so there's a lot of hardcoded stuff and other not so good programming practices.

# Getting started
Portal Bot comes with examples for WPI, Rice, Case Western, Rose-Hulman, RIT, Northeastern, and Olin.

If any schools use Slate by Technoloutions (most US schools do), you can easily copy and paste one of the schools to represent a new one, just by changing the url.

In the first few lines of code, make sure the executable path of the web driver is set correctly.
There are hints in the code for where to put passwords & usernames in the portal, along with URLs as well.

Setting up Slack is a little trickier. You'll need a Slack workspace for Portal Bot, and you'll need to create an app to represent portal bot (visit api.slack.com). Create a new app. Get the Bot OAuth token from OAuth & permissions, and paste it into the bottom part of the code where the Slack uploads occur.

On the same page, you'll need to add the chat:write, files:write, and incoming-webhook token scopes so that the bot can work. Finally, install the app in the channel where Portal Bot will post messages in. Make sure this channel matches the channel in the bottom part of the code for the channel name.

Test and make sure portal bot works. By default, it's headless, but you can make it not headless by setting the option to be headless in the code to false. It's helpful to run it not headless the first time so that you can identify if a password is wrong, etc, before letting it run headless.

# Extending Portal Bot
Portal Bot should, in theory, work with most portals. If you learn a bit about how Selenium works, and you can start identifying field & button IDs and such, you can extend portal bot to any college you wish.

Some schools may not initially work with Portal Bot, even if they use Slate by Technolutions or Salesforce. Some schools have their own signin system that will require you to determine what fields to put usernames/passwords in, before logging into their system.

# License
Portal Bot is licensed under the MIT License. It has been relicensed from the Do What The F*ck Public License on May 16th, 2020 to a more widely recognized license.
