# Portal Bot
# (c) 2020 o355 under the MIT License

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time
import requests
chrome_options = Options()
chrome_options.headless = True
chrome_options.add_argument("--disable-features=VizDisplayCompositor")
driver = webdriver.Chrome(executable_path='/put/path/to/web/driver/here', options=chrome_options)

### START WPI CAPTURE ###
print("Starting WPI capture...")
driver.set_window_size(1080, 1500)
driver.get("https://wpicommunity.force.com/apply/TX_SiteLogin?startURL=%2Fapply%2FTargetX_Base__Portal")
ueElement = driver.find_element_by_id("j_id0:j_id1:siteLogin:loginComponent:loginForm:username")
ueElement.send_keys("Put your login email address here")
pwElement = driver.find_element_by_id("j_id0:j_id1:siteLogin:loginComponent:loginForm:password")
pwElement.send_keys("Put your password here")
driver.find_element_by_name("j_id0:j_id1:siteLogin:loginComponent:loginForm:submitForm").click()
time.sleep(8)
pageheight = driver.execute_script("return document.body.scrollHeight")
driver.set_window_size(1080, pageheight)
driver.save_screenshot("wpi.png")
print("WPI capture complete.")

### START RICE CAPTURE ###
driver.set_window_size(1080, 2200)
driver.get("https://riceadmission.rice.edu/account/login")
ueElement = driver.find_element_by_id("email")
ueElement.send_keys("Put your login email address here")
pwElement = driver.find_element_by_id("password")
pwElement.send_keys("Put your password here")
pwElement.send_keys(Keys.ENTER)
time.sleep(5)
driver.save_screenshot("rice.png")

### START CWRU CAPTURE ###
print("Starting CWRU capture...")
driver.set_window_size(1080, 3500)
driver.get("https://go.case.edu/apply/status")
ueElement = driver.find_element_by_id("username")
ueElement.send_keys("Put your username here")
pwElement = driver.find_element_by_id("password")
pwElement.send_keys("Put your password here")
driver.find_element_by_id("login-submit").click()
time.sleep(5)
pageheight = driver.execute_script("return document.body.scrollHeight")
driver.set_window_size(1080, pageheight)
driver.save_screenshot("cwru.png")
print("CWRU capture complete.")

### START ROSE HULMAN CAPTURE ###
print("Starting Rose-Hulman capture (3/6)...")
driver.set_window_size(1080, 3290)
driver.get("https://admissions.rose-hulman.edu/apply/status")
ueElement = driver.find_element_by_id("email")
ueElement.send_keys("Put your login email address here")
pwElement = driver.find_element_by_id("password")
pwElement.send_keys("Put your password here")
pwElement.send_keys(Keys.ENTER)
time.sleep(5)
pageheight = driver.execute_script("return document.body.scrollHeight")
driver.set_window_size(1080, pageheight)
driver.save_screenshot("rose.png")
print("Rose-Hulman capture complete.")

### START RIT CAPTURE ###
print("Starting RIT capture...")
driver.set_window_size(1080, 1800)
driver.get("https://join.rit.edu/account/login?r=%2fapply%2fstatus")
ueElement = driver.find_element_by_id("email")
ueElement.send_keys("Put your login email address here")
pwElement = driver.find_element_by_id("password")
pwElement.send_keys("Put your password here")
pwElement.send_keys(Keys.ENTER)
time.sleep(5)
pageheight = driver.execute_script("return document.body.scrollHeight")
driver.set_window_size(1080, pageheight)
driver.save_screenshot("rit.png")
print("RIT capture complete.")

## START NORTHEASTERN CAPTURE ###
print("Starting Northeastern capture...")
driver.set_window_size(1080, 4000)
driver.get("https://ugadmissions.northeastern.edu/ApplicantApp/ApplicantAppLogin.asp")
ueElement = driver.find_element_by_name("userid")
ueElement.send_keys("Put your NU ID here")
pwElement = driver.find_element_by_name("userdob")
pwElement.send_keys("Put your password here")
driver.find_element_by_id("login").click()
time.sleep(2)
try:
    ueElement = driver.find_element_by_name("userid")
    ueElement.send_keys("Put your NU ID here")
    pwElement = driver.find_element_by_name("userdob")
    pwElement.send_keys("Put your password here")
    driver.find_element_by_id("login").click()
except:
    pass

time.sleep(8)
pageheight = driver.execute_script("return document.body.scrollHeight")
driver.set_window_size(1080, pageheight)
driver.save_screenshot("northeastern.png")
print("Northeastern capture complete.")

### START OLIN CAPTURE ###
print("Starting Olin capture...")
driver.set_window_size(1080, 1800)
driver.get("https://admission.olin.edu/account/login?r=%2fapply%2fstatus")
ueElement = driver.find_element_by_id("email")
ueElement.send_keys("Put your login address here")
pwElement = driver.find_element_by_id("password")
pwElement.send_keys("Put your password here")
pwElement.send_keys(Keys.ENTER)
time.sleep(5)
pageheight = driver.execute_script("return document.body.scrollHeight")
driver.set_window_size(1080, pageheight)
driver.save_screenshot("olin.png")
print("Olin capture complete.")


### SLACK FUN
print("Uploading to Slack...")
r = requests.post('https://slack.com/api/files.upload', data={'token': "Put your bot user OAuth Token here",
                                                              'channels': ["#portals"], 'initial_comment': 'Here is the latest portal check for WPI.', 'title': "WPI Portal"}, files={'file': open("wpi.png", "rb")})

r = requests.post('https://slack.com/api/files.upload', data={'token': "Put your bot user OAuth Token here",
                                                              'channels': ["#portals"], 'initial_comment': 'Here is the latest portal check for Rice.', 'title': "Rice Portal"}, files={'file': open("rice.png", "rb")})

r = requests.post('https://slack.com/api/files.upload', data={'token': "Put your bot user OAuth Token here",
                                                              'channels': ["#portals"], 'initial_comment': 'Here is the latest portal check for Northeastern.', 'title': "Northeastern Portal"}, files={'file': open("northeastern.png", "rb")})

r = requests.post('https://slack.com/api/files.upload', data={'token': "Put your bot user OAuth Token here",
                                                              'channels': ["#portals"], 'initial_comment': 'Here is the latest portal check for Rose Hulman.', 'title': "Rose Hulman Portal"}, files={'file': open("rose.png", "rb")})

r = requests.post('https://slack.com/api/files.upload', data={'token': "Put your bot user OAuth Token here",
                                                              'channels': ["#portals"], 'initial_comment': 'Here is the latest portal check for Case Western.', 'title': "Case Western Portal"}, files={'file': open("cwru.png", "rb")})

r = requests.post('https://slack.com/api/files.upload', data={'token': "Put your bot user OAuth Token here",
                                                              'channels': ["#portals"], 'initial_comment': 'Here is the latest portal check for RIT.', 'title': "RIT Portal"}, files={'file': open("rit.png", "rb")})

r = requests.post('https://slack.com/api/files.upload', data={'token': "Put your both user OAuth Token here",
                                                              'channels': ["#portals"], 'initial_comment': 'Here is the latest portal check for Olin.', 'title': "Olin Portal"}, files={'file': open("olin.png", "rb")})
print("Killing driver...")
driver.quit()
print("Done!")
